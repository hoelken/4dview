import logging
import numpy as np
from astropy.io import fits

log = logging.getLogger('INVVIEW')
log.setLevel(logging.INFO)
ch = logging.StreamHandler()
ch.setLevel(logging.INFO)
ch.setFormatter(logging.Formatter('%(asctime)s - %(levelname)s - %(message)s'))
log.addHandler(ch)


class BaseLoader:
    # Supported Keys
    ALPHA = 'ALPHA'
    AZIMU = 'AZIMU'
    BFIEL = 'BFIEL'
    DBDLT = 'DBDLT'
    CHISQ = 'CHISQ'
    GAMMA = 'GAMMA'
    LGTRF = 'LGTRF'
    TEMPE = 'TEMPE'
    VELOS = 'VELOS'
    VGRAD = 'VGRAD'
    VMACI = 'VMACI'
    VMICI = 'VMICI'

    WLREF = 'WLREF'
    WLMIN = 'WLMIN'
    WLMAX = 'WLMAX'
    NWL = 'NWL'

    KEYS = []

    CMAPS = {TEMPE: 'jet',
             VELOS: 'coolwarm', VGRAD: 'coolwarm', VMICI: 'viridis', VMACI: 'viridis',
             BFIEL: 'jet', DBDLT: 'coolwarm', GAMMA: 'hsv', AZIMU: 'hsv',
             ALPHA: 'gray', CHISQ: 'viridis'}

    def __init__(self, path: str):
        self.folder = path
        self.key_map = {}
        self.nnodes = 0
        self.lgtau = []
        self.values_glob = {}
        self.values_nodes = {}
        self.obs_wl = None
        self.obs = None
        self.profs_wl = None
        self.profs = None
        self.wlref = 0
        self.wlmin = 0
        self.wlmax = 0
        self.weights = []
        #: Init these in subclass
        self._atmos = ''
        self._lam = ''
        self._obs = ''
        self._profs = ''
        self._weight_i = ''
        self._weight_q = ''
        self._weight_u = ''
        self._weight_v = ''

    def read(self, weights: bool = False):
        self.read_atmos()
        self.read_lam()
        self.read_obs()
        self.read_profs()
        if weights:
            self.read_weights()

    def read_atmos(self):
        log.info("Reading %s", self._atmos)
        with fits.open(self._atmos) as hdul:
            print(repr(hdul[0].header))
            self._build_keymap(hdul[0].header)
            self._extract_all_values(hdul[0].data)

        log.info('Found %i log(tau) nodes [%s]', self.nnodes, self.LGTRF)
        log.info('Found %i height dependent nodes: %s', len(self.values_nodes), self.values_nodes.keys())
        log.info('Found %i height independent nodes: %s', len(self.values_glob), self.values_glob.keys())

    def _build_keymap(self, header):
        for k in self.KEYS:
            i = 0
            ids = []
            comments = []
            more = True
            while more:
                try:
                    ids.append(int(header[(k, i)]) - 1)
                    comments.append(header.comments[(k, i)])
                    i += 1
                except (IndexError, KeyError):
                    more = False
            if i > 0:
                self.key_map[k] = {'ids': ids, 'comments': comments, 'len': i}
        self.nnodes = self.key_map[self.LGTRF]['len']

    def _extract_all_values(self, data: np.array) -> None:
        if not self.key_map:
            raise RuntimeError('Key Map not initialized. Call "build_keymap(header)" first...')

        for k in self.key_map.keys():
            v = self._extract_values(k, data)
            if len(v) == 1:
                self.values_glob[k] = v[0]
            else:
                self.values_nodes[k] = v

        self.lgtau = [self.values_nodes[self.LGTRF][j][0, 0] for j in range(self.nnodes)]
        del self.values_nodes[self.LGTRF]

    def _extract_values(self, key: str, data: np.array) -> list:
        return [data[i] for i in self.key_map[key]['ids']]

    def read_lam(self):
        log.info("Reading %s", self._lam)
        with fits.open(self._lam) as hdul:
            wlref = float(hdul[0].header[self.WLREF])
            self.obs_wl = hdul[0].data[0, 0] + wlref

    def read_obs(self):
        log.info("Reading %s", self._obs)
        with fits.open(self._obs) as hdul:
            self.obs = hdul[0].data

    def read_profs(self):
        log.info("Reading %s", self._profs)
        with fits.open(self._profs) as hdul:
            self.wlref = float(hdul[0].header[self.WLREF])
            self.wlmin = float(hdul[0].header[self.WLMIN])
            self.wlmax = float(hdul[0].header[self.WLMAX])
            nwl = int(hdul[0].header[self.NWL])
            self.profs = hdul[0].data
            self.profs_wl = np.linspace(self.wlref + self.wlmin, self.wlref + self.wlmax, nwl)

    def read_weights(self):
        raise NotImplemented("The BaseLoader class has no knowledge about weights. Implemented in derived classes")
