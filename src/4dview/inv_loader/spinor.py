import os
from glob import glob

import numpy as np
from astropy.io import fits
from lib.sorting import sort_num

from .base import log, BaseLoader


class SpinorLoader(BaseLoader):
    """
    Loader class for SPINOR inversion results
    """

    # Supported keys
    KEYS = [
        BaseLoader.LGTRF,
        BaseLoader.TEMPE,
        BaseLoader.VELOS,
        BaseLoader.VGRAD,
        BaseLoader.VMICI,
        BaseLoader.VMACI,
        BaseLoader.BFIEL,
        BaseLoader.DBDLT,
        BaseLoader.GAMMA,
        BaseLoader.AZIMU,
        BaseLoader.ALPHA,
        BaseLoader.CHISQ
    ]

    def __init__(self, path: str, component: int = 1):
        super().__init__(path)
        self.component = component
        self._atmos = os.path.join(path, 'inverted_atmos.fits')
        self._lam = os.path.join(path, 'inverted_lam.*.fits')
        self._obs = os.path.join(path, 'inverted_obs.*.fits')
        self._profs = os.path.join(path, 'inverted_profs.*.fits')
        self._weight_i = os.path.join(path, 'weights_i.fits')
        self._weight_q = os.path.join(path, 'weights_q.fits')
        self._weight_u = os.path.join(path, 'weights_u.fits')
        self._weight_v = os.path.join(path, 'weights_v.fits')

    def _build_keymap(self, header):
        super()._build_keymap(header)
        self._filter_keymap()

    def _filter_keymap(self):
        log.info('Filter extracted keys for atmospheric component %i', self.component)
        for (k, v) in self.key_map.items():
            self._filter_ids_for_component(k, v)
            self._reject_empty_keys()
        self.nnodes = self.key_map[self.LGTRF]['len']

    def _filter_ids_for_component(self, key: str, info: dict) -> None:
        if key in [self.ALPHA, self.CHISQ]:
            return

        ids = []
        comments = []
        for i in range(info['len']):
            c = info['comments'][i].split()
            if int(c[3]) == self.component:
                ids.append(info['ids'][i])
                comments.append(info['comments'][i])
        self.key_map[key] = {'ids': ids, 'comments': comments, 'len': len(ids)}

    def _reject_empty_keys(self):
        self.key_map = {k: v for k, v in self.key_map.items() if v['len'] > 0}

    def read_weights(self):
        for f in [self._weight_i, self._weight_v, self._weight_q, self._weight_u]:
            log.info("Reading %s", f)
            weights = fits.getdata(f)
            weights[0] = weights[0] + self.wlref
            self.weights.append(weights)

    def read_lam(self):
        data = []
        for file in sort_num(glob(self._lam)):
            log.info("Reading %s", file)
            with fits.open(file) as hdul:
                wlref = float(hdul[0].header[self.WLREF])
                data.append(hdul[0].data[0, 0] + wlref)
        self.obs_wl = np.concatenate(data)

    def read_obs(self):
        data = []
        for file in sort_num(glob(self._obs)):
            log.info("Reading %s", file)
            data.append(fits.getdata(file))
        # self.obs = np.concatenate(data, axis=3)
        self.obs = self._concat(data)

    def read_profs(self):
        data = []
        wlpoints = []

        for file in sort_num(glob(self._profs)):
            log.info("Reading %s", file)
            with fits.open(file) as hdul:
                wlref = float(hdul[0].header[self.WLREF])
                wlmin = float(hdul[0].header[self.WLMIN])
                wlmax = float(hdul[0].header[self.WLMAX])
                nwl = int(hdul[0].header[self.NWL])
                data.append(hdul[0].data)
                wlpoints.append(np.linspace(wlref + wlmin, wlref + wlmax, nwl))
        self.profs_wl = np.concatenate(wlpoints)
        self.profs = self._concat(data)
        self.wlref = np.mean(self.profs_wl)
        self.wlmin = np.min(self.profs_wl)
        self.wlmax = np.max(self.profs_wl)

    @staticmethod
    def _concat(data: list) -> np.array:
        nwl = np.sum([d.shape[3] for d in data])
        merged = np.zeros((data[0].shape[0], data[0].shape[1], data[0].shape[2], nwl))
        s = 0
        for d in data:
            e = d.shape[3]
            merged[:, :, :, s:s + e] = d
            s += e
        return merged
