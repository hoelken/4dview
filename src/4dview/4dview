#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from argparse import ArgumentParser
from dataclasses import dataclass

from astropy.io import fits
import matplotlib.pyplot as plt
from matplotlib.widgets import Slider
from mpl_toolkits.axes_grid1 import make_axes_locatable

from string_utils import parse_shape

aparser = ArgumentParser(description='Interactive viewer for 4d cubes')
aparser.add_argument('path', type=str, help='Path to the image')
aparser.add_argument("-r", "--roi", type=str, nargs="?", default='[:,:,:]', help='[Y,X,WL] ROI in numpy notation.')
aparser.add_argument("-s", "--sigma", type=float, nargs="?", default=5, help='Sigma value for clim')
aparser.add_argument("-w", "--width", type=int, nargs="?", default=1400, help='pixel width of the main plot')
aparser.add_argument("--wlrange", type=int, nargs="?", default=4, help='Number of wl pixels to average for the imgs')
aparser.add_argument("--4d", dest="mode4d", action="store_true", help='Use 4D mode')
args = aparser.parse_args()

roi = parse_shape(args.roi)
DPI = 120
PO = [0, 1, 2, 3]
R = args.wlrange // 2

mode4d = args.mode4d or '.4d.' in args.path.lower()
if mode4d:
    # Change the parameter order matching 4d fits files
    # from [s, y, x, wl] to [y, x, s, wl]
    PO = [2, 0, 1, 3]
    print("INFO", "4D.Fits mode activated...")


@dataclass
class LiveConf:
    x: int = 0
    y: int = 0
    wl: int = 0
    zoomed: bool = False
    sigma: float = 0

    def __repr__(self):
        return f'LiveConf(x={self.x}, y={self.y}, wl={self.wl}, sigma={self.sigma:.2f}, zoomed={self.zoomed})'


def imclick(event):
    if 'zoom' in fig1.canvas.toolbar.mode:
        return

    if event.button == 1 and event.xdata is not None and event.ydata is not None:
        c.y = int(event.xdata)
        c.x = int(event.ydata)
        redraw()


def spclick(event):
    if event.inaxes == sp_axs[4]:
        return
    if 'zoom' in fig2.canvas.toolbar.mode:
        return

    if event.button == 1 and event.xdata is not None:
        c.wl = int(event.xdata)
        redraw()


def imhome(event):
    for ax in im_axs:
        ax.relim()
        ax.autoscale_view()
    plt.figure(1)
    plt.draw()


def sphome(_evt):
    for ax in sp_axs:
        ax.relim()
        ax.autoscale_view()
    plt.figure(2)
    plt.draw()


def lims_change(_evt):
    c.zoomed = True


def redraw():
    draw()
    plt.figure(1)
    plt.draw()
    plt.figure(2)
    plt.draw()


def update_sigma(_val):
    c.sigma = sigma_slider.val
    redraw()


def cbar(cax, color):
    fig1.colorbar(color, cax=cax)


def draw():
    for i, s in enumerate(['I', 'Q/I', 'U/I', 'V/I']):
        ax = im_axs[i]
        xlim = ax.get_xlim()
        ylim = ax.get_ylim()
        ax.clear()
        wl = c.wl + roi[2].start if roi[2] and roi[2].start is not None else c.wl
        wlr = slice(wl-R, wl+R+1)
        im = f.data[roi[0], roi[1], i, wlr] if mode4d else f.data[i, roi[0], roi[1], wlr]
        im = im.mean(axis=2)
        clim = None
        if c.sigma <= 5:
            m = im.mean()
            r = c.sigma * im.std()
            clim = [m - r, m + r]
        color = ax.imshow(im, cmap='gray', clim=clim)
        cbar(caxs[i], color)
        ax.plot(c.y, c.x, '+', color='red')
        ax.set_ylabel(s)
        if c.zoomed:
            ax.set_xlim(xlim)
            ax.set_ylim(ylim)

        ax = sp_axs[i]
        xlim = ax.get_xlim()
        ax.clear()
        y = c.y + roi[0].start if roi[0] and roi[0].start is not None else c.y
        d = f.data[c.x, y, i, roi[2]] if mode4d else f.data[i, c.x, y, roi[2]]
        ax.plot(d)
        ax.axvline(x=c.wl-R, color='red', linestyle='--', linewidth=0.7)
        ax.axvline(x=c.wl+R+1, color='red', linestyle='--', linewidth=0.7)
        ax.set_ylabel(s)
        if c.zoomed:
            ax.set_xlim(xlim)
        # r = 0.5 * d.std()
        # ax.set_ylim([d.min() - r, d.max() + r])
    sp_axs[-1].set_xlabel("wl [px]")
    fig1.subplots_adjust(top=0.995, bottom=0.1, left=0.05, right=0.945, hspace=0, wspace=0)
    fig2.tight_layout()


print("INFO", 'Reading', args.path)
with fits.open(args.path) as hdul:
    hdul[0].verify('silentfix')
    f = hdul[0]
    print("INFO", "data shape", f.data.shape)

    ratio = f.data.shape[PO[1]] * 4 / f.data.shape[PO[2]]
    fig1, im_axs = plt.subplots(nrows=4, sharex='all', sharey='all',
                                figsize=(args.width / DPI, args.width * ratio / DPI), dpi=DPI)
    fig2, sp_axs = plt.subplots(nrows=5, gridspec_kw={'height_ratios': [6, 6, 6, 6, 1]})

    for j in range(1, 4):
        sp_axs[j].sharex(sp_axs[0])
    for j in range(3):
        sp_axs[j].tick_params(top=False, labeltop=False, bottom=True, labelbottom=False)

    caxs = []
    for j in range(4):
        divider = make_axes_locatable(im_axs[j])
        caxs.append(divider.append_axes("right", size="0.5%", pad=0.03))

    sigma_slider = Slider(
        ax=sp_axs[4],
        label='Sigma',
        valmin=0.5,
        valmax=5.5,
        valinit=args.sigma,
    )

    xpos = min(5, f.data.shape[PO[2]]-1)
    ypos = min(5, f.data.shape[PO[1]]-1)
    wlpos = min(5, f.data.shape[PO[3]]-1)
    c = LiveConf(xpos, ypos, wlpos, sigma=args.sigma)
    # print("NOTE", repr(c))
    draw()

    fig1.canvas.mpl_connect('button_press_event', imclick)
    fig2.canvas.mpl_connect('button_press_event', spclick)
    sigma_slider.on_changed(update_sigma)

    for a in im_axs:
        a.callbacks.connect('ylim_changed', lims_change)
    for b in sp_axs:
        b.callbacks.connect('ylim_changed', lims_change)
    plt.show()
    plt.draw()
