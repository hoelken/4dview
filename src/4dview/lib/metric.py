import numpy as np
from scipy.signal import resample

from .maths import save_divide


def _resample(obs, obs_wl, fit, fit_wl, y, x, i) -> tuple:
    obs = obs[y, x, i]
    fit = fit[y, x, i]
    w0 = np.argmin(np.abs(fit_wl - obs_wl.min()))
    w1 = np.argmin(np.abs(fit_wl - obs_wl.max()))
    return obs, resample(fit[w0:w1], len(obs))


def chi2(obs: np.array, obs_wl: np.array, fit: np.array, fit_wl: np.array, y: int, x: int, i: int) -> np.array:
    obs, fit = _resample(obs, obs_wl, fit, fit_wl, y, x, i)
    return save_divide((obs - fit)**2, obs**2)


def diff(obs: np.array, obs_wl: np.array, fit: np.array, fit_wl: np.array, y: int, x: int, i: int) -> np.array:
    obs, fit = _resample(obs, obs_wl, fit, fit_wl, y, x, i)
    return np.abs(obs - fit)
