import numpy as np


def save_divide(a: np.array, b: np.array) -> np.array:
    """Divides a by b but ignoring 0 values"""
    return np.true_divide(a.astype('float32'), b.astype('float32'),
                          out=a.astype('float32'), where=b != 0, dtype='float64')
