# Plotting/inspection library for data cubes.

Supports `*.fits` based data cubes. Two viewers are included:

- **4DView** for Stokes data cubes
- **InvView** for inversion results

Clone the git repo, install requirements (see [`Pipfile`](Pipfile)).
Minimum python version is `Python 3.8`.

**Author** hoelken@mps.mpg.de   
Contributions are very welcome. 

## 4DView
Image viewer for `*.fits` based Stokes data cubes.
WL calibration is ignored. everything is displayed in pixels...

The default dimensions are `stokes x x-axis x y-axis x wl-axis`.  
If the files matches `*.4d.*` or the `--4d` flag is given via command line the 4d data 
format (e.g. for HINODE, SPINOR, Helix, ...) is assumed.

### Open cube
Opening a cube is easy: 
```bash
./4dview /path/to/cube.fits
```

The ROI is given in typical numpy notation, e.g. via `--roi "[20:800,90:1600]"`

You can find all up-to-date help and usage information using the `-h` switch.

The cube is NOT fully loaded, only the part needed for plotting is read 
(this allows to open X00 GB cubes with a low memory footprint). 
However, with very big data cubes it can still take a while to jump to the right position. Be patient. ;)


### Interactive GUI
The program will open two `matplotlib` figure windows. 
One containing the four 2D images of the Stokes parameters, the other containing the spectra for the selected pixel.
![img.png](img.png)

Zooming is supported in both figures. The sigma slider sets the clipping range.

- Clicking on the spectra will select this WL in the 2d Images.
- Clicking in the image will select this position for the spectra.

If "zoom" or "drag/move" mode is activated, clicking will not change pixel or wavelength position. 
Both positions are indicated by red markings (`+` or `|`). 

 

## InvView
Interactive viewer for `*.fits` based inversion results.   
Currently only SPINOR results are supported
 - with arbitrary number of log(tau) nodes.
 - with multiple components

### Open results
Usage is simple: Provide a path to a folder with the inversion results.
The following files must be present:
- inverted_atmos.fits
- inverted_lam.1.fits
- inverted_obs.1.fits
- inverted_profs.1.fits

```bash
./invview /path/to/inversion/results/folder -x 23 -y 42
```
x and y position is optional, but nice if you already know the pixel you are interested in. 
You can find all up-to-date help and usage information using the `-h` switch.

### Interactive GUI
The program will open several `matplotlib` figure windows.
- Observed profiles and fits for the selected pixel position
- Stratification of the height dependent values for the selected pixel (no interpolation yet)
- 2D maps of all height dependent values per log(tau) node 
- 2D maps of all global values

![invview.png](invview.png)

As for `4dview` clicking in one of the 2D maps will select the pixel and update all plots accordingly.
The selected pixel is indicated with a black plus (`+`) in all maps.

All matplotlib interactive tools are supported.
If "zoom" or "drag/move" mode is activated, clicking will not change pixel position. 